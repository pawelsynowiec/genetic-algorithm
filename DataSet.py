import os
import numpy as np
from operator import itemgetter


class DataSet:
    @staticmethod
    def calculate_distance(city1, city2):
        return np.sqrt(np.power(city1[1] - city2[1], 2) + np.power(city1[2] - city2[2], 2))

    def __init__(self, filePath):
        if not os.path.exists(filePath):
            print('File does not exist.')
        self.cities = []
        self.items = []
        self.no_of_cities = 0
        self.no_of_items = 0
        with open(filePath, 'r') as f:
            content = f.readlines()
        for i, l in enumerate(content):
            if i == 2:
                self.no_of_cities = int(l.split('\t')[1].replace('\n', ''))
            if i == 3:
                self.no_of_items = int(l.split('\t')[1].replace('\n', ''))
            if i == 4:
                self.knapsack = int(l.split('\t')[1].replace('\n', ''))
            if i == 5:
                self.min_speed = float(l.split('\t')[1].replace('\n', ''))
            if i == 6:
                self.max_speed = float(l.split('\t')[1].replace('\n', ''))
            if 9 < i < 10 + self.no_of_cities:
                removed_and_splitted = l.replace('\n', '').split('\t')
                self.cities.append(
                    [int(removed_and_splitted[0])-1, float(removed_and_splitted[1]), float(removed_and_splitted[2])])
            if (10 + self.no_of_cities) < i < (11 + self.no_of_cities + self.no_of_items):
                removed_and_splitted = l.replace('\n', '').split('\t')
                self.items.append(
                    [int(removed_and_splitted[0]), int(removed_and_splitted[1]), int(removed_and_splitted[2]),
                     int(removed_and_splitted[3]) -1, float(removed_and_splitted[1]) / float(removed_and_splitted[2])])
        self.items = sorted(self.items, key=itemgetter(4), reverse=True)
        self.matrix_distances = []
        counter = 0
        for city in self.cities:
            temp = []
            internal_counter = 0
            while internal_counter < counter:
                temp.append(DataSet.calculate_distance(city, self.cities[internal_counter]))
                internal_counter += 1
            self.matrix_distances.append(temp)
            counter += 1
        self.greedy_pick = []
        space_left = self.knapsack

        for item in self.items:
            i = 0
            found = False
            if item[2] <= space_left and space_left - item[2] >= 0:
                while i < len(self.greedy_pick):
                    if item[3] == self.greedy_pick[i][3]:
                        found = True
                        break
                    i += 1
                if not found:
                    self.greedy_pick.append(item)
                    space_left -= item[2]