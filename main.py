from DataSet import DataSet
from Genotype import Genotype
from operator import itemgetter
import matplotlib.pyplot as plt
import random
import numpy
import copy
import time
import cProfile as profile
import os

# In outer section of code
pr = profile.Profile()
pr.enable()


# selection always best from all
#
# def selection(population):
#     elite = []
#     for i in range(size):
#         tournament = []
#         for chosen in random.sample(range(size), Tour):
#             tournament.append(population[chosen])
#         tournament = sorted(tournament, key=itemgetter(1))
#         elite.append(copy.deepcopy(tournament[0]))
#     return elite

def selection(population):
    elite = []
    for i in range(int(len(population) / 2)):
        tournament = []
        for chosen in random.sample(range(len(population)), Tour):
            tournament.append(population[chosen])
        tournament = sorted(tournament, key=itemgetter(1))
        elite.append(copy.deepcopy(tournament[0]))
        population.remove(tournament[0])
    return elite


def get_random():
    mu, sigma = 0.5, 0.2  # mean and standard deviation
    return numpy.random.normal(mu, sigma)


def crossover(population, Px):
    crossed = []
    for i in range(0, len(population), 2):
        if get_random() < Px:
            temp1 = copy.deepcopy(population[i][0])
            temp2 = copy.deepcopy(population[i + 1][0])
            for j in range(int(len(temp1) / 2)):
                population[i][0][population[i][0].index(temp2[j])], population[i][0][j] = population[i][0][j], \
                                                                                          population[i][0][
                                                                                              population[i][0].index(
                                                                                                  temp2[j])]
                population[i + 1][0][population[i + 1][0].index(temp1[j])], population[i + 1][0][j] = \
                    population[i + 1][0][j], population[i + 1][0][population[i + 1][0].index(temp1[j])]
        tp1, tp2 = Genotype(dataset, population[i][0]), Genotype(dataset, population[i + 1][0])
        crossed.append([tp1.path, tp1.efficiency])
        crossed.append([tp2.path, tp2.efficiency])
    return crossed


def mutation(population, dataset, Pm):
    mutated = []
    for i in range(len(population)):
        if get_random() < Pm:
            swap = random.sample(range(len(population[i][0])), 2)
            population[i][0][swap[0]], population[i][0][swap[1]] = population[i][0][swap[1]], population[i][0][swap[0]]
        tp = Genotype(dataset, population[i][0])
        mutated.append([tp.path, tp.efficiency])
    return population


def add_to_file(population, agen):
    score = []
    with open('results_' + file.split('.')[0] + '_try_' + str(tr) + '.csv', 'a') as fd:
        if agen == 0:
            fd.write("GEN;SCORE\n")
        for element in population:
            fd.write(str(agen) + ';' + str(element[1]).replace('.', ',') + '\n')
            score.append(element[1])
    data_to_plot[0].append(agen)
    data_to_plot[1].append(min(score))
    data_to_plot[2].append(sum(score) / len(score))
    data_to_plot[3].append(max(score))
    return agen + 1


def plot_population():
    plt.plot(data_to_plot[0], data_to_plot[3], 'r-', label='max')
    plt.plot(data_to_plot[0], data_to_plot[2], 'b-', label='avg')
    plt.plot(data_to_plot[0], data_to_plot[1], 'g-', label='min')
    plt.grid(True)
    plt.xlabel('Generation')
    plt.ylabel('Efficiency')
    plt.legend(loc='upper right')
    plt.title('Evolution ' + file.split('.')[0] + ' try ' + str(tr))
    plt.savefig('results_' + file.split('.')[0] + '_try_' + str(tr) + '.png')
    plt.show()


for file in os.listdir('dataSet'):
    for tr in range(1):
        dataset = DataSet('dataSet/' + file)
        current_population = []
        populations = []
        data_to_plot = [[], [], [], []]
        agen = 0
        gen = 100
        size = 100
        Tour = 5
        Px = 0.01
        Pm = 0.01
        start_time = time.time()

        # init
        print('Initializing... ' + file + ' #' + str(tr))
        for i in range(size):
            genotype = Genotype(dataset, random.sample(range(dataset.no_of_cities), dataset.no_of_cities))
            current_population.append([genotype.path, genotype.efficiency])
        agen = add_to_file(current_population, agen)
        populations.append(copy.deepcopy(current_population))

        # loop

        for i in range(gen - 1):
            current_population = copy.deepcopy(selection(current_population))  # returns half
            current_population = copy.deepcopy(crossover(current_population, Px) + current_population)
            # current_population = copy.deepcopy(crossover(current_population, Px))
            current_population = copy.deepcopy(mutation(current_population, dataset, Pm))
            agen = add_to_file(current_population, agen)
            populations.append(current_population)

            print(
                '{:6.2f} % | {:6.2f} / {:6.2f} s | {:.2f} - {:.2f} - {:.2f}'.format(float(agen) / float(gen) * 100, time.time() - start_time,
                                                                                     (time.time() - start_time) / agen * gen,
                                                                                     data_to_plot[1][-1], data_to_plot[2][-1],
                                                                                     data_to_plot[3][-1]))
            if agen % 10 == 0:
                plot_population()
        print('DONE')

pr.disable()
pr.dump_stats('profile.pstat')