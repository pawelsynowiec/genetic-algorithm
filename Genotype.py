import random


class Genotype:

    def calculate_efficiency(self, current_speed, efficiency, city1, city2, dataset, picks_left):

        for pick in picks_left:
            if pick[3] == city1:
                current_speed -= ((dataset.max_speed - dataset.min_speed) * (pick[2] / dataset.knapsack))
                picks_left.remove(pick)
                break
        efficiency = efficiency + (dataset.matrix_distances[city1][city2] / current_speed)
        return current_speed, efficiency

    def __init__(self, dataset, path):
        self.path = path
        self.efficiency = 0
        self.current_speed = dataset.max_speed
        picks_left = dataset.greedy_pick
        for i in range(0, dataset.no_of_cities - 1):
            if self.path[i] > self.path[i + 1]:
                self.current_speed, self.efficiency = self.calculate_efficiency(self.current_speed, self.efficiency,
                                                                                self.path[i], self.path[i + 1], dataset,
                                                                                picks_left)
            else:
                self.current_speed, self.efficiency = self.calculate_efficiency(self.current_speed, self.efficiency,
                                                                                self.path[i + 1], self.path[i], dataset,
                                                                                picks_left)

    def __getitem__(self, index):
        return self.path[index]